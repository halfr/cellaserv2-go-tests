// dummy sender
package main

import (
	"bitbucket.org/evolutek/cellaserv2-protobuf"
	"code.google.com/p/goprotobuf/proto"
	"encoding/binary"
	"fmt"
	"net"
	"os"
	"runtime/pprof"
	"time"
)

func main() {
	conn, err := net.Dial("tcp", "localhost:4200")
	if err != nil {
		panic("Could not connect")
	}

	f, err := os.Create("profile")
	if err != nil {
		panic(err)
	}
	pprof.StartCPUProfile(f)
	defer pprof.StopCPUProfile()

	for i := 0; i < 10000; i += 1 {
		begin := time.Now()
		msgRequest := &cellaserv.Request{}
		service := "date"
		msgRequest.ServiceName = &service
		method := "time"
		msgRequest.Method = &method
		id := uint64(i)
		msgRequest.Id = &id
		msgRequestBytes, err := proto.Marshal(msgRequest)
		if err != nil {
			panic(err)
		}

		msgType := cellaserv.Message_Request
		msg := &cellaserv.Message{}
		msg.Type = &msgType
		msg.Content = msgRequestBytes
		msgBytes, err := proto.Marshal(msg)
		if err != nil {
			panic(err)
		}

		binary.Write(conn, binary.BigEndian, uint32(len(msgBytes)))
		conn.Write(msgBytes)

		var msgLen uint32
		err = binary.Read(conn, binary.BigEndian, &msgLen)
		if err != nil {
			panic(err)
		}

		msgBytes = make([]byte, msgLen)
		_, err = conn.Read(msgBytes)
		if err != nil {
			panic(err)
		}

		err = proto.Unmarshal(msgBytes, msg)
		if err != nil {
			panic(err)
		}

		if *msg.Type != cellaserv.Message_Reply {
			continue
		}

		msgReply := &cellaserv.Reply{}
		err = proto.Unmarshal(msg.Content, msgReply)
		if err != nil {
			panic(err)
		}
		fmt.Sprintln(time.Since(begin))

	}
}

// vim: set sw=8 noet:
