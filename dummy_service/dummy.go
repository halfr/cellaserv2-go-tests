// dummy sender
package main

import (
	"bitbucket.org/evolutek/cellaserv2-protobuf"
	"code.google.com/p/goprotobuf/proto"
	"encoding/binary"
	"fmt"
	"net"
	"time"
)

func main() {
	serviceName := "date"
	msgRegister := &cellaserv.Register{
		Name: &serviceName,
	}

	msgRegisterBytes, err := proto.Marshal(msgRegister)
	if err != nil {
		panic("Could not marshal message")
	}

	msgType := cellaserv.Message_Register
	msg := &cellaserv.Message{
		Type:    &msgType,
		Content: msgRegisterBytes,
	}

	msgBytes, err := proto.Marshal(msg)
	if err != nil {
		panic("Could not marshal message")
	}

	conn, err := net.Dial("tcp", "localhost:4200")
	if err != nil {
		panic("Could not connect")
	}

	msgLen := uint32(len(msgBytes))
	err = binary.Write(conn, binary.BigEndian, msgLen)
	if err != nil {
		panic(fmt.Sprint("Binary write failed:", err))
	}

	_, err = conn.Write(msgBytes)
	if err != nil {
		panic(fmt.Sprint("Binary write failed:", err))
	}

	// Main loop
	for {
		err = binary.Read(conn, binary.BigEndian, &msgLen)
		if err != nil {
			panic(err)
		}
		//begin := time.Now()

		msgBytes = make([]byte, msgLen)
		_, err = conn.Read(msgBytes)
		if err != nil {
			panic(err)
		}

		err = proto.Unmarshal(msgBytes, msg)
		if err != nil {
			panic(err)
		}

		if *msg.Type != cellaserv.Message_Request {
			panic("Not a request")
		}

		msgRequest := &cellaserv.Request{}
		err = proto.Unmarshal(msg.Content, msgRequest)
		if err != nil {
			panic(err)
		}

		if *msgRequest.Method != "time" {
			panic("Bad method")
		}

		msgReply := &cellaserv.Reply{}
		msgReply.Data = []byte(time.Now().String())
		msgReply.Id = msgRequest.Id
		msgReplyBytes, err := proto.Marshal(msgReply)
		if err != nil {
			panic(err)
		}

		msgType = cellaserv.Message_Reply
		msg.Type = &msgType
		msg.Content = msgReplyBytes
		msgBytes, err = proto.Marshal(msg)
		if err != nil {
			panic(err)
		}

		binary.Write(conn, binary.BigEndian, uint32(len(msgBytes)))
		conn.Write(msgBytes)
		//fmt.Println(time.Since(begin))
	}
}
